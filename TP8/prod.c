#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "shared.h"

int main(int argc, char const *argv[])
{
	
	if(argc <= 1) {
		printf("Usage: %s <char>\n", argv[0]);
		exit(1);
	}

	// Initialisation des sémaphores
	int mutexRead = sem_create((key_t) MUTEX_READ, 1);
	int mutexWrite = sem_create((key_t) MUTEX_WRITE, 1);
	int semFull = sem_create((key_t) SEM_FULL, BUFFER_SIZE);
	int semEmpty = sem_create((key_t) SEM_EMPTY, 0);

	char value = argv[1][0];

	// Bloqué si plein
	P(semFull); 

	// Lit et incrémente l'index d'écriture
	P(mutexWrite);

	// Récupération mémoire partagée	
	int memId;
	if((memId = shmget(SHARED_MEM_KEY, BUFFER_SIZE + 2, 0)) == -1) {
		perror("Unable to get shared memory");
		exit(-1);
	}

	char *mem;
	if((mem = shmat(memId, 0, 0)) == (char*)-1) {
		perror("Unable to access shared memory");
		exit(-2);
	}

	// Lecture index d'écriture
	int curIdx = mem[WR_IDX];

	// Ecriture du prochain index d'écriture
	mem[WR_IDX] = (curIdx + 1) % BUFFER_SIZE; // Prochain indice d'écriture

	// Rend l'accès à la case stockant l'index d'écriture
	V(mutexWrite);

	// Ecriture à l'index précédemment récupéré
	mem[curIdx] = value;

	// On rempli le sémaphore semEmpty pour que les lecteurs puissent lire
	V(semEmpty);

	printf("Writed %c @ %d", value, curIdx);

	// Libération de la mémoire
	if(shmdt(mem) == -1) {
		perror("Unable to detach mem\n");
	}


	return 0;
}