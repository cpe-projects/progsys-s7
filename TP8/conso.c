#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "shared.h"

int main(int argc, char const *argv[])
{

	// Initialisation des sémaphores
	int mutexRead = sem_create((key_t) MUTEX_READ, 1);
	int mutexWrite = sem_create((key_t) MUTEX_WRITE, 1);
	int semFull = sem_create((key_t) SEM_FULL, BUFFER_SIZE);
	int semEmpty = sem_create((key_t) SEM_EMPTY, 0);

	// Bloqué si vide
	P(semEmpty); 

	// Lit et incrémente l'index de lecture
	P(mutexRead);

	// Récupération mémoire partagée	
	int memId;
	if((memId = shmget(SHARED_MEM_KEY, BUFFER_SIZE + 2, 0)) == -1) {
		perror("Unable to get shared memory");
		exit(-1);
	}

	char *mem;
	if((mem = shmat(memId, 0, 0)) == (char*)-1) {
		perror("Unable to access shared memory");
		exit(-2);
	}

	// Lecture index de lecture
	int curIdx = mem[RD_IDX];

	// Ecriture du prochain index de lecture
	mem[RD_IDX] = (curIdx + 1) % BUFFER_SIZE; // Prochain indice de lecture

	// Rend l'accès à la case stockant l'index de lecture
	V(mutexRead);

	// Lecture à l'index précédemment récupéré
	char value = mem[curIdx];

	// On rempli le sémaphore semFull pour que les écrivains puissent écrire
	V(semFull);

	printf("Readen %c @ %d", value, curIdx);

	// Libération de la mémoire
	if(shmdt(mem) == -1) {
		perror("Unable to detach mem\n");
	}


	return 0;
}