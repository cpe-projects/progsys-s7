#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "shared.h"

int main(int argc, char const *argv[])
{
	
	// Initialisation des sémaphores
	int mutexRead = sem_create((key_t) MUTEX_READ, 1);
	int mutexWrite = sem_create((key_t) MUTEX_WRITE, 1);
	int semFull = sem_create((key_t) SEM_FULL, BUFFER_SIZE);
	int semEmpty = sem_create((key_t) SEM_EMPTY, 0);

	// Création d'une mémoire partagée avec X cases + deux cases pour stocker les têtes de lecture
	int memId;

	if((memId = shmget(SHARED_MEM_KEY, BUFFER_SIZE + 2, 0)) > 0) {
		if(shmctl(memId, IPC_RMID, 0) == -1) {
			perror("Unable to delete shared memory");
			exit(-2);
		}
	}

	if((memId = shmget(SHARED_MEM_KEY, BUFFER_SIZE + 2, IPC_CREAT | IPC_EXCL | 0666)) == -1) {
		perror("Unable to create shared memory\n");
		exit(-1);
	}

	// Accès à la mémoire partagée
	char *mem;
	if((mem = shmat(memId, 0, 0)) == (char*)-1) {
		perror("Unable to access shared memory");
		exit(-2);
	}

	// Initialisation des indices
	mem[WR_IDX] = 0;
	mem[RD_IDX] = 0;

	if(shmdt(mem) == -1) {
		perror("Unable to detach mem\n");
	}

	printf("Shared memory, key=%d, id=%d\n", SHARED_MEM_KEY, memId);

	return 0;
}