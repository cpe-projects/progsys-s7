#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "shared.h"

int main(int argc, char const *argv[])
{
	
	// Initialisation des sémaphores
	int mutexRead = sem_create((key_t) MUTEX_READ, 0);
	sem_delete(mutexRead);

	int mutexWrite = sem_create((key_t) MUTEX_WRITE, 0);
	sem_delete(mutexWrite);

	int semFull = sem_create((key_t) SEM_FULL, 0);
	sem_delete(semFull);

	int semEmpty = sem_create((key_t) SEM_EMPTY, 0);
	sem_delete(semEmpty);


	// Création d'une mémoire partagée avec X cases + deux cases pour stocker les têtes de lecture
	int memId;
	if((memId = shmget(SHARED_MEM_KEY, BUFFER_SIZE + 2, 0)) == -1) {
		perror("Unable to get shared memory");
		exit(-1);
	}

	printf("Shared memory, key=%d, id=%d\n", SHARED_MEM_KEY, memId);

	if(shmctl(memId, IPC_RMID, 0) == -1) {
		perror("Unable to delete shared memory");
		exit(-2);
	}

	printf("Deleted shared memory\n");

	return 0;
}