#include "lib/dijkstra.h"
#include <sys/shm.h>
#include <sys/ipc.h>

#define MUTEX_READ 1
#define MUTEX_WRITE 2
#define SEM_FULL 3
#define SEM_EMPTY 4

#define SHARED_MEM_KEY 1

#define BUFFER_SIZE 5

#define WR_IDX BUFFER_SIZE
#define RD_IDX (BUFFER_SIZE+1)