#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "lib/dijkstra.h"

#define SEM_A 001
#define SEM_B 002

int main(int argc, char const *argv[])
{
	
	// Initialisation des sémaphores
	int semA = sem_create((key_t) SEM_A, 0);
	int semB = sem_create((key_t) SEM_B, 0);

	printf("B: Bonjour, je suis B et je pars.\n");
	sleep(5);
	printf("B: Je suis arrivé, j'attends mon pote.\n");

	V(semA); // J'attends mon pote
	P(semB); // Je suis arrivé

	printf("B: Cool, A est arrivé !\n");

	return 0;
}