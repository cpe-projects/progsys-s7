#include <stdlib.h>
#include <stdio.h>
#include "lib/dijkstra.h"

#define SEM_RCV 003
#define SEM_EMIT 004
#define SEM_MUTEX_EMIT 005

int main(int argc, char const *argv[])
{
	
	int mutex = sem_create((key_t) SEM_RCV, 0);
	sem_delete(mutex);

	printf("Deleted mutex %d", mutex);

	mutex = sem_create((key_t) SEM_EMIT, 0);
	sem_delete(mutex);

	mutex = sem_create((key_t) SEM_MUTEX_EMIT, 0);
	sem_delete(mutex);

	printf("Deleted mutex %d", mutex);

	return 0;
}

