#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "lib/dijkstra.h"

#define SEM_RCV 003
#define SEM_EMIT 004

#define N 3

int main(int argc, char const *argv[])
{
	
	// Sémaphores communs aux émetteurs/récepteurs
	// -> l'émetteur attend que les récepteurs lui aient
	//	  envoyé chacun un jeton
	int semEmit = sem_create((key_t) SEM_EMIT, 0);

	// -> les récepteurs produisent chacun un jeton
	//	  pour l'émetteur
	int semRcv = sem_create((key_t) SEM_RCV, 0);

	printf("Receiver: Je suis récepteur.\n");
	sleep(5);

	printf("Receiver: Tiens, un jeton\n");
	V(semRcv);
	
	printf("Receiver: Stp donne moi le go\n");
	P(semEmit);


	printf("Receiver: Je me casse !\n");

	return 0;
}