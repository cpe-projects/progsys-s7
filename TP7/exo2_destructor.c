#include <stdlib.h>
#include <stdio.h>
#include "lib/dijkstra.h"

#define SEM_A 001
#define SEM_B 002

int main(int argc, char const *argv[])
{
	
	// Récupère le mutex créé par T1 (car T1 est censé être exécuté avant)
	int mutex = sem_create((key_t) SEM_A, 0);
	sem_delete(mutex);

	printf("Deleted mutex %d", mutex);

	mutex = sem_create((key_t) SEM_A, 0);
	sem_delete(mutex);

	printf("Deleted mutex %d", mutex);

	return 0;
}