#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "lib/dijkstra.h"

#define SEM_RCV 003
#define SEM_EMIT 004
#define SEM_MUTEX_EMIT 005

#define N 3

int main(int argc, char const *argv[])
{
	
	// Sémaphores communs aux émetteurs/récepteurs
	// -> l'émetteur attend que les récepteurs lui aient
	//	  envoyé chacun un jeton
	int semEmit = sem_create((key_t) SEM_EMIT, 0);

	// -> les récepteurs produisent chacun un jeton
	//	  pour l'émetteur
	int semRcv = sem_create((key_t) SEM_RCV, 0);

	// -> permet à l'émetteur de bloquer les autres émetter
	// 	  jusqu'à ce que les récepteurs soient là
	int semMutex = sem_create((key_t) SEM_MUTEX_EMIT, 1);

	P(semMutex);

	printf("Emitter: Je suis émetteur.\n");
	sleep(5);
	printf("Emitter: Svp les récepteurs, donnez jetons\n");

	for (int i = 0; i < N-1; ++i)
	{
		P(semRcv);
	}

	for (int i = 0; i < N-1; ++i)
	{
		V(semEmit);
	}

	printf("Emitter: On se casse !\n");

	V(semMutex);

	return 0;
}