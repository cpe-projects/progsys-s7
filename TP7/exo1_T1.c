#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "lib/dijkstra.h"

#define MUTEX 123

int main(int argc, char const *argv[])
{
	
	// Initialisation d'un mutex à 0
	int mutex = sem_create((key_t) MUTEX, 0);

	printf("Bonjour, je suis T1 et je travaille beaucoup trop (10s) et c'est dur.\n");
	sleep(10);

	V(mutex); // Place un jeton dans le mutex

	return 0;
}