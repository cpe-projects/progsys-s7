#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "lib/dijkstra.h"

#define SEM_A 001
#define SEM_B 002

int main(int argc, char const *argv[])
{
	
	// Initialisation des sémaphores
	int semA = sem_create((key_t) SEM_A, 0);
	int semB = sem_create((key_t) SEM_B, 0);

	printf("A: Bonjour, je suis A et je pars.\n");
	sleep(10);
	printf("A: Je suis arrivé, j'attends mon pote.\n");

	V(semB); // J'attends mon pote
	P(semA); // Je suis arrivé

	printf("A: Cool, B est arrivé !\n");

	return 0;
}