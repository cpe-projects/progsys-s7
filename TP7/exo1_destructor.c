#include <stdlib.h>
#include <stdio.h>
#include "lib/dijkstra.h"

#define MUTEX 123

int main(int argc, char const *argv[])
{
	
	// Récupère le mutex créé par T1 (car T1 est censé être exécuté avant)
	int mutex = sem_create((key_t) MUTEX, 0);
	sem_delete(mutex);

	printf("Delete mutex %d", mutex);

	return 0;
}