#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "lib/dijkstra.h"

#define MUTEX 123

int main(int argc, char const *argv[])
{
	
	// Récupère le mutex créé par T1 (car T1 est censé être exécuté avant)
	int mutex = sem_create((key_t) MUTEX, 0);

	P(mutex); // Attend que T1 donne un jeton 

	printf("Bonjour, je suis T2 et je travaille beaucoup trop aussi (5s).\n");
	sleep(5);

	V(mutex); // Rend le jeton

	return 0;
}