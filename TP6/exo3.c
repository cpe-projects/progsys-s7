#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

int my_bool = 0;

void sig_handler(int n) {
	my_bool = 1;
	printf("boolean is now true\n");
	exit(0);
}

int main(int argc, char const *argv[])
{
	signal(SIGINT, sig_handler);
	for(;;);
	return 0;
}