#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <fcntl.h>

void sig_handler(int sig)
{
    printf("\nTime is over!\n");
    exit(0);
}

int main(int argc, char const *argv[])
{

	char number[50];


    signal(SIGALRM, sig_handler);
    alarm(5);
	
	for(;;) {
		printf("Input number: ");
		scanf("%s", number);

		int num = atoi(number);

		if(num > 0)
			printf("Hye %d\n", num);

	}

	return 0;
}