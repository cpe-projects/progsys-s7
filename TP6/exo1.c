#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

void sig_handler(int n) {
	printf("Salut");
	exit(0);
}

int main(int argc, char const *argv[])
{
	signal(SIGINT, sig_handler);
	for(;;);
	return 0;
}