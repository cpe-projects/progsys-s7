#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <fcntl.h>

void sig_handler(int n) {
	if(n == SIGUSR1) {
		printf("SIGUSR1");
	} else if(n == SIGUSR2) {
		printf("Naaan on me kill\n");
		exit(0);
	}
}

int main(int argc, char const *argv[])
{

	int pid = fork();

	if(pid == 0) {
		int i;
		signal(SIGUSR1, sig_handler);
		signal(SIGUSR2, sig_handler);

		for(;;) {
			printf("Fils\n");
			sleep(1);
		}

	} else {
		int i; 

		for(i = 0; i < 5; i++) {
			printf("Père\n");
			if(i == 2)
				kill(pid, SIGUSR1);
			else if(i == 4)
				kill(pid, SIGUSR2);

			sleep(1);
		}
	}

	return 0;
}