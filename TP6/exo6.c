#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <fcntl.h>

void sig_handler(int n) {
	printf("Naaan on me kill\n");
	exit(0);
}

/**
 * Si on fait CTRL+C dans le père
 * le signal va être envoyé au fils
 * => on voit 'NAaan on me kill'
 * MAIS comme ça kill le père, le fils
 * va être kill
 **/
int main(int argc, char const *argv[])
{

	int pid = fork();

	if(pid == 0) {
		
		// Interception des SIGINT par le fils
		signal(SIGINT, sig_handler);
		for(;;);

	}

	for(;;);

	return 0;
}