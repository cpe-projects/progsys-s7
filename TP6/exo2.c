#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

int main(int argc, char const *argv[])
{
	signal(SIGINT, SIG_IGN);
	for(;;);
	return 0;
}