#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <fcntl.h>

int main(int argc, char const *argv[])
{

	int pid = fork();

	if(pid == 0) {
		int i; 

		for(;;) {
			printf("Fils\n");
			sleep(1);
		}

	} else {
		int i; 

		for(i = 0; i < 3; i++) {
			printf("Père\n");
			sleep(1);
		}

		kill(pid, SIGKILL);
	}

	return 0;
}