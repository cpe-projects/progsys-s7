#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char const *argv[])
{
	
	int status;

	if(fork() == 0) {
		printf("Je suis le fils %d\n", getpid());

		int pid = execlp("/bin/ls", "ls", "-l", NULL);
		
		/*char *arg[2];
		arg[0] = "ls";
		arg[1] = NULL;
		execv("/bin/ls", arg);

		printf("Papa, j'ai fini\n");*/

		exit(-1);
	}

	int pid_child;
	pid_child = wait(&status);
	printf("Je suis le père et mon fils avait pour pid %d %d\n", pid_child, WEXITSTATUS(status));

	return 0;
}