#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char *argv[])
{
	
	int status;
	int all_ok = 1;


	// 1 = Compiling c files in parallel
	for (int i = 1; i < argc; i++)
	{
		char *buf;

		sprintf(buf, "%s.c", argv[i]);

		if(access(buf, F_OK) != -1) { // Checks if file exists
			printf("File %s exists\n", buf);
			if(fork() != 0) {
				printf("Executing gcc\n");
				execlp("/usr/bin/gcc", "gcc", "-c", buf, NULL);

				printf("gcc fails\n");
				exit(-1); // Called if execlp fails
			}
		}

	}

	// 2 = Linking every .o
	// Wait for every children
	for(int i = 1; i < argc; i++) {
		wait(&status);

		if(WEXITSTATUS(status) == -1) // If sth fails
			all_ok = 0;
	}

	if(all_ok) { // If everything ok
		int gcc_argc = argc-1+3;
		char *gcc_args[gcc_argc];

		printf("%d args", gcc_argc);

		strcpy(gcc_args[0], "gcc");
		strcpy(gcc_args[1], "-o");
		strcpy(gcc_args[2], "programme");

		int i = 0;

		for (int i = 1; i < argc; i++)
		{
			char *buf;

			sprintf(buf, "%s.o", argv[i]);
			printf("buf is %s", buf);
			//files[i+2] = buf;
			
		}

		files[i+2] = NULL;

		execv("/usr/bin/gcc", files);
	}

	return 0;
}