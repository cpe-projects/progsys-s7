#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char const *argv[])
{
	
	int status;
	int all_ok = 1;

	if(argc < 2)
		exit(-1);

	int nb_fork = atoi(argv[1]);

	for (int i = 0; i < nb_fork; i++)
	{
		if(fork() == 0) {
			printf("pid=%d, ppid=%d\n", getpid(), getppid());
			sleep(2*i);
			exit(i);
		}

	}

	while(wait(&status) != -1);

	printf("Tout fini");

	return 0;
}