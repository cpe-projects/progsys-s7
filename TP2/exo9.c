#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char const *argv[])
{

	int status, pid1, pid2, pid3, pid4;
	
	for (int i = 0; i < 3; i++)
	{
		fork();
		fork();
	}

	printf(".");

	waitpid(-1, NULL, 0);

	return 0;
}