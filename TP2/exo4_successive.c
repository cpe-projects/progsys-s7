#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char const *argv[])
{
	int status;

	if(fork() == 0) 
		execlp("/usr/bin/who", "who", NULL);
	
	wait(&status);

	if(fork() == 0)
		execlp("/bin/ps", "ps", NULL);
	
	wait(&status);

	if(fork() == 0)
		execlp("/bin/ls", "ls", "-l", NULL);
	
	wait(&status);


	return 0;
}