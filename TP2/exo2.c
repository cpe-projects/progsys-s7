#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char const *argv[])
{
	
	for (int i = 0; i < 3; i++)
	{
		int fork_ret = fork();
		int pid = getpid();
		int ppid = getppid();

		printf("%d :: fork = %d; pid=%d; ppid=%d\n", i, fork_ret, pid, ppid);
		sleep(1);
	}

	return 0;
}