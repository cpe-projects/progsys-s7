#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

int main(int argc, char const *argv[])
{
	
	int pid;
	char data;

	int df1 = open("exo3_data/entree", O_RDONLY);
	int df2 = creat("exo3_data/sortie", 0666);

	FILE *trace = fopen("exo3_data/trace", "w");

	fprintf(trace, "Le caractère '#' indique que c'est le processus fils qui s'exécute\n");
	fprintf(trace, "Le caractère '$' indique que c'est le processus père qui s'exécute\n");

	fflush(trace);

	pid = fork();

	printf("Création de processus\n");
	while(read(df1, &data, sizeof(char)) > 0) {
		if(pid == 0)
			fprintf(trace, "$%c", data);
		else
			fprintf(trace, "#%c", data);
		fflush(trace);
		write(df2, &data, sizeof(char));
	}

	printf("End of while loop\n");
	close(df1);
	close(df2);

	return 0;
}