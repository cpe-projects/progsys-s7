#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char const *argv[])
{
	int status;

	// Parallèle
	if(fork() == 0) { // Fils 1

		if(fork() == 0) { // Fils 2
			execlp("/usr/bin/who", "who", NULL);
		}

		execlp("/bin/ps", "ps", NULL);
	}
	
	execlp("/bin/ls", "ls", "-l", NULL);
	
	wait(&status);
	wait(&status);

	return 0;
}