#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char const *argv[])
{
	
	int i, delay;

	for(i = 0; i<4; i++) if(fork()) break;

	srand(getpid());

	delay = rand()%4;
	sleep(delay);

	printf("Mon nom est est %c, j'ai dormi %d secondes\n", 'E'-delay, delay);
	exit(EXIT_SUCCESS);

	return 0;
}