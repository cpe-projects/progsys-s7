#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/fcntl.h>
#include <sys/stat.h>
#include <time.h>

#define OPEN(P, N, S) P = open(N, S);
#define WRITE(P, B, S) write(P, B, S);					
#define READ(P, B, S)  read(P, B, S);
#define CLOSE(P) close(P);

#define UNLINK_NAMED_PIPE(N) unlink(N);
#define CREATE_NAMED_PIPE(N) mkfifo(N, 0644);

#define REQUEST_PIPE_NAME "Client2Server"
#define BUF_SIZE 256

int main(int argc, char const *argv[])
{

	UNLINK_NAMED_PIPE(REQUEST_PIPE_NAME)
	CREATE_NAMED_PIPE(REQUEST_PIPE_NAME)
	
	char buffer[BUF_SIZE];
	int requestPipe;

	while(1) {	

		OPEN(requestPipe, REQUEST_PIPE_NAME, O_RDONLY)


		printf("Waiting for data\n");
		READ(requestPipe, buffer, BUF_SIZE)
		printf("Received value from pid %s\n", buffer);

		CLOSE(requestPipe)

	}


	return 0;
}