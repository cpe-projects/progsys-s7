#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/fcntl.h>
#include <sys/stat.h>
#include <time.h>

#define OPEN(P, N, S) P = open(N, S);
#define WRITE(P, B, S) if(write(P, B, S) != S) { perror("Error while writing pipe #P"); exit(2); }					
#define READ(P, B, S) if(read(P, B, S) != S) { perror("Error while reading pipe #P"); exit(3); }
#define CLOSE(P) if(close(P) == 0) { perror("Error while closing pipe"); exit(4); }

#define UNLINK_NAMED_PIPE(N) unlink(N);
#define CREATE_NAMED_PIPE(N) mkfifo(N, 0644);

#define RESPONSE_PIPE_NAME "Server2Client_"
#define REQUEST_PIPE_NAME "Client2Server"

#define BUF_SIZE 256

int main(int argc, char const *argv[])
{

	char responsePipeName[BUF_SIZE];
	sprintf(responsePipeName, "%s%d", RESPONSE_PIPE_NAME, 1);

	//UNLINK_NAMED_PIPE(responsePipeName)
	//CREATE_NAMED_PIPE(responsePipeName)
	
	char buffer[BUF_SIZE];
	int responsePipe, requestPipe;

	//OPEN(responsePipe, responsePipeName, O_RDONLY)
	OPEN(requestPipe, REQUEST_PIPE_NAME, O_WRONLY)

	WRITE(requestPipe, "responsePipeName", strlen("responsePipeName"))
	printf("Value sent to server : %s\n", "responsePipeName");

	/*WRITE(requestPipe, "2+2", strlen("2+2"))
	printf("Expression sent : 2+2");*/

	//CLOSE(responsePipe)

	return 0;
}