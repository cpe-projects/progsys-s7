#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <time.h>

#define BUF_SIZE 256
#define MAX_VALUE 500

#define OPEN_PIPE(P) if(pipe(P) != 0) { perror("Error while opening pipe #"); exit(1); }
#define WRITE_PIPE(P, B, S) if(write(P, B, S) != S) { perror("Error while writing pipe #P"); exit(2); }					
#define READ_PIPE(P, B, S) if(read(P, B, S) != S) { perror("Error while reading pipe #P"); exit(2); }

int getPrevProcessNo(int curProcNo, int nbProcesses) {
	return (curProcNo == 0) ? nbProcesses - 1 : curProcNo - 1;
}

int getNextProcessNo(int curProcNo, int nbProcesses) {
	return (curProcNo == nbProcesses - 1) ? 0 : curProcNo + 1;
}

int main(int argc, char const *argv[])
{

	if(argc < 2) {
		printf("Usage: %s X\n", argv[0]);
		return -1;
	}

	int nbProcesses = atoi(argv[1]);

	if(nbProcesses <= 1) {
		printf("Min: 2 processes\n");
		return -1;
	}

	int pipes[nbProcesses][2];
	char buffer[BUF_SIZE];
	int status;
	
	// Le père ouvre tous les pipes
	for (int i = 0; i < nbProcesses; i++)
	{	
		OPEN_PIPE(pipes[i])
	}

	for (int i = 0; i < nbProcesses; i++)
	{	
		if(fork() == 0) { // Fils
			srand(time(NULL) ^ (getpid()<<16));

			int nextProc = getNextProcessNo(i, nbProcesses);
			int prevProc = getPrevProcessNo(i, nbProcesses);

			// Fermeture des pipes inutilisés par le processus courant
			for(int j = 0; j < nbProcesses; j++) {
				if(j != prevProc && j != nextProc) {
					close(pipes[j][0]);
					close(pipes[j][1]);
				}
			}

			if(nbProcesses > 2) { // On ne ferme pas si il y a 2 processus : chacun gérera son truc
				close(pipes[nextProc][0]); // Osef du suivant en lecture
				close(pipes[prevProc][1]); // Osef du précédent en écriture
			}

			// Génération d'un nombre aléatoire
			int value = rand() % MAX_VALUE;
			printf("I'm process n°%d. My PID is %d and my value is %d\n", i, getpid(), value);

			if(i == 0) { // Cas du processus 0 (qui génère le premier nombre)

				sprintf(buffer, "%d", value);
				WRITE_PIPE(pipes[nextProc][1], buffer, BUF_SIZE)
				printf("%d write value %s to %d\n", i, buffer, nextProc);

				READ_PIPE(pipes[prevProc][0], buffer, BUF_SIZE)
				printf("%d read value %s from %d\n", i, buffer, prevProc);

				printf("THE HIGHEST VALUE IS %s\n", buffer);

			} else {
				READ_PIPE(pipes[prevProc][0], buffer, BUF_SIZE)
				printf("%d read value %s from %d\n", i, buffer, prevProc);

				int precVal = atoi(buffer);
				if(precVal < value) {
					sprintf(buffer, "%d", value); // On modifie le buffer ssi notre valeur est > précédente
				}

				WRITE_PIPE(pipes[nextProc][1], buffer, BUF_SIZE)
				printf("%d write value %s to %d\n", i, buffer, nextProc);
			}
			
			exit(0);
		}
	}

	return 0;
}