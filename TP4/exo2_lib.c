#include "exo2_lib.h"

int creer_tube() {
    return mkfifo("TubeNomme", S_IRWXU | S_IRWXG | S_IRWXO);
}

void detruire_tube() {
    unlink("TubeNomme");
}

void lecture() {
    int tube, longueur;
    char message[50];
    tube = open("TubeNomme", O_RDONLY);
    longueur = read(tube, message, 30);
    message[longueur] = '\0';
    
    printf("%d a lu le message\n\t\t%s\n", getpid(), message);
    
    if(close(tube)) {
        perror("Error while closing pipe");
        exit(1);
    }

}

void ecriture() {
    int tube;
    char message[50];
    sprintf(message, "processus %d", getpid());
    tube = open("TubeNomme", O_WRONLY);
    write(tube, message, strlen(message));
    close(tube);
}