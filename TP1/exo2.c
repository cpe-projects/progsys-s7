#include <stdio.h>
#include <string.h>

int main(int argc, char *argv[]) {

    int i;

    if(argc < 2)
        return 1;

    i=strlen(argv[1]);

    for(i; i >= 0; i--)
        printf("%c", argv[1][i]);
    
    printf("\n");
    return 0;
    
}
