#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {

    int i, j;

    if(argc < 2) {
        printf("Aucune moyenne à calculer\n");    
        return 1;
    }

    int sum = 0;
    int nb_vals = argc-1;
    int cur_num;

    for(i=1; i < argc; i++) {
        cur_num = atoi(argv[i]);
        
        if(cur_num < 0 || cur_num > 20) {
            printf("Note non valide\n");
            return 2;
        }

        sum += cur_num;
    }

    float mean = (float)sum/(float)nb_vals; // cast en float pour forcer le calcul des décimales
    
    printf("La moyenne est : %.2f", mean);

    printf("\n");
    return 0;
    
}
