#include <stdio.h>
#include <string.h>
#include <stdlib.h>

extern char **environ;

int main(int argc, char *argv[]) {
    
    if(argc < 2)    // Aucun argument
        return 1;

    char *key = argv[1]; // On recherche la variable d'env ayant pour nom le paramètre
    sprintf(key, "%s=", key); // On cherchera si la variable est CLE=....

    char **env;
    
    printf("Looking for key %s\n", key);

    for(env = environ; *env != 0; env++) { // Parcours du tableau d'env
        char *value = *env; // Variable courante (ex: USER=laurent)
        
        char *substr = strstr(value, key);
        if(substr != NULL) { // Si la clé cherchée est présente
            int substr_pos = substr - value; // Position de départ de la clé cherchée
            
            if(substr_pos == 0) {
                int key_len = strlen(key); // Taille de la clé (avec le "=")
                int val_len = strlen(value) - key_len; // Taille de la valeur (à droite du "=")
                printf("%.*s\n", val_len, value + key_len); // Nb à afficher, position de départ (décalage de pointeur)
            }
        }
    };
    
    return 0;
    
}
