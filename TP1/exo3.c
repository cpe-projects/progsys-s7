#include <stdio.h>
#include <string.h>

int main(int argc, char *argv[]) {

    int i, j;

    if(argc < 2)
        return 1;

    for(i=1; i < argc; i++) {
        printf("\n");
        for(j=strlen(argv[i]); j >= 0; j--)
            printf("%c", argv[i][j]);
    }

    printf("\n");
    return 0;
    
}
