#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <pthread.h>
#include <semaphore.h>

#define BUFFER_SIZE 5

pthread_mutex_t mutexWrite = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutexRead = PTHREAD_MUTEX_INITIALIZER;

struct data {
	int buffer[BUFFER_SIZE];
	int readIndice, writeIndice;
	sem_t semRead;
	sem_t semWrite; 
};
typedef struct data Data;

/**
 * Thread "Producteur"
 * Attend un argument de type "Data"
 **/
void *prod(void *arg) {
	Data *data = (Data*) arg;
	int value = rand() % 100;
	
	sem_wait(&data->semWrite); // On attend d'avoir des cases à remplir

	pthread_mutex_lock(&mutexWrite);  // Prise du jeton d'accès à l'index d'écriture
	int indice = data->writeIndice; // Incrémentation de l'index d'écriture
	data->writeIndice = (indice+1)%BUFFER_SIZE; 
	pthread_mutex_unlock(&mutexWrite); // Relâchement du jeton d'accès à l'index d'écriture

	data->buffer[indice] = value; // Ecriture à l'index writeIndice
	printf("P : wrote %d at indice %d\n", value, indice);

	sem_post(&data->semRead); // On notifie les lecteurs qu'une case supplémentaire est à lire

	return NULL;
}

/**
 * Thread "Consommateur"
 * Attend un argument de type "Data"
 **/
void *conso(void *arg) {
	Data *data = (Data*) arg;

	sem_wait(&data->semRead);  // Attend d'avoir une case à lire

	pthread_mutex_lock(&mutexRead); // Prise du jeton d'accès à l'index de lecture
	int indice = data->readIndice;
	data->readIndice = (indice+1)%BUFFER_SIZE; // Incrémentation de l'index de lecture
	pthread_mutex_unlock(&mutexRead); // Relâchement du jeton d'accès à l'index de lecture

	int value = data->buffer[indice]; // Lit la case
	printf("C : read %d at indice %d\n", value, indice);
	
	sem_post(&data->semWrite); // Notifie les producteurs qu'une case est à remplire

	return NULL;
}

int main(int argc, char const *argv[])
{
	if (argc != 3) {
		printf("Usage : %s nb_prod nb_conso\n", argv[0]);
		exit(-1);
	}

	int nbProd = atoi(argv[1]);
	int nbConso = atoi(argv[2]);

	// Tableau des threads
	pthread_t prodIds[nbProd];
	pthread_t consoIds[nbConso];
	
	// Initialisation des données
	Data data;
	data.readIndice=0;
	data.writeIndice=0;
	sem_init(&(data.semRead), 0, 0);
	sem_init(&(data.semWrite), 0, BUFFER_SIZE);

	srand(time(NULL));

	// Création des "Producteurs"
	for(int i = 0; i<nbProd; i++) {
		pthread_create(prodIds+i, NULL, prod, &data);
	}

	// Création des "Consommateurs"
	for(int i = 0; i<nbConso; i++) {
		pthread_create(consoIds+i, NULL, conso, &data);
	}

	// Attente des "Producteurs"
	for(int i = 0; i<nbProd; i++) {
		pthread_join(prodIds[i], NULL);
	}

	// Attente des "Consommateurs"
	for(int i = 0; i<nbConso; i++) {
		pthread_join(consoIds[i], NULL);
	}

	// Suppression des sémaphores
	pthread_mutex_destroy(&mutexWrite);
	pthread_mutex_destroy(&mutexRead);

	sem_destroy(&(data.semRead));
	sem_destroy(&(data.semWrite));

	return 0;
}