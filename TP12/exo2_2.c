#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <pthread.h>

#define NB_THREADS 2

struct dataStruct {
	char **filenames;
	int curIdx;
	int nbFiles;
};

typedef struct dataStruct Data;

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void process(char *filename) {

	FILE *fp1, *fp2;
	char c = '.'; // Initilisé à '.' car le compilateur ou l'OS lui réaffecte l'ancienne valeur (donc EOF)..

	fp1 = fopen(filename, "r");
	fp2 = fopen(filename, "r+");

	printf("Processing %s\n", filename);

	if(fp1 == NULL || fp2 == NULL) {
		perror("fopen");
		exit(1);
	}

	while(c != EOF) {

		c = fgetc(fp1);
		if(c != EOF) fputc(toupper(c), fp2);

	}


	fclose(fp1);
	fclose(fp2);

}

void *worker(void *arg) {

	Data *data = (Data*) arg;
	int index = 0;
	int nbFiles = data->nbFiles;

	while(index <= nbFiles) {

		pthread_mutex_lock(&mutex);  // Prise du jeton d'accès à l'index
		index = data->curIdx; // Incrémentation de l'index
		data->curIdx = index+1;
		pthread_mutex_unlock(&mutex); // Relâchement du jeton d'accès à l'index d'écriture

		if(index <= nbFiles) {
			printf("Working @ %d : %s\n", index, data->filenames[index]);
			process(data->filenames[index]);
		}

	}

	return NULL;

}

int main (int argc, char **argv) {
	
	if(argc < 2) {
		printf("Usage: %s file1 file2 ...\n", argv[0]);
		exit(0);
	}

	pthread_t workers[NB_THREADS];

	Data data;
	data.filenames = argv;
	data.curIdx = 1; // Car argv[0] est le nom du programme
	data.nbFiles = argc-1;

	printf("Processing %d files with %d threads\n", data.nbFiles, NB_THREADS);

	for(int i = 0; i < NB_THREADS; i++) {
		pthread_create(workers+i, NULL, worker, &data);
	}

	for(int i = 0; i < NB_THREADS; i++) {
		pthread_join(workers[i], NULL);
	}

	pthread_mutex_destroy(&mutex);

	return 0;

}