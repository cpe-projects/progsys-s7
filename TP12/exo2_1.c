#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <pthread.h>

/**
 * Thread worker
 * Attend un argument de type char*
 **/
void* worker(void *arg) {
	char *filename = (char*) arg;

	FILE *fp1, *fp2;
	int c;

	fp1 = fopen(filename, "r");
	fp2 = fopen(filename, "r+");

	if(fp1 == NULL || fp2 == NULL) {
		perror("fopen");
		exit(1);
	}

	while(c != EOF) {
		c = fgetc(fp1);
		if(c != EOF) fputc(toupper(c), fp2);
	}

	fclose(fp1);
	fclose(fp2);

	return NULL;
}

int main (int argc, char **argv) {
	
	if (argc < 2) {
		printf("Usage : %s file1 file2 ...\n", argv[0]);
		exit(-1);
	}

	int nbWorkers = argc-1;

	// Tableau des threads
	pthread_t workers[nbWorkers];

	// Création des workers
	for(int i=0; i<nbWorkers; i++) {
		pthread_create(workers+i, NULL, worker, argv[i+1]);
	}

	// Attente des workers
	for(int i=0; i<nbWorkers; i++) {
		pthread_join(workers[i], NULL);
	}

	return 0;

}