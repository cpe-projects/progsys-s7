#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <pthread.h>

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void *fct(void *arg) {
	int no = *((int *) arg);

	printf("Thread %d attend...\n", no);
	pthread_mutex_lock(&mutex); // Attend d'avoir la main

	printf("Thread %d travaille...\n", no);
	sleep(no);
	printf("Thread %d a fini...\n", no);

	pthread_mutex_unlock(&mutex); // Débloque le suivant

	return NULL;
}

int main(int argc, char const *argv[])
{

	if(argc < 2) {
		printf("Usage %s nb_proc\n", argv[0]);
		exit(2);
	}

	pthread_mutex_lock(&mutex); // Blocage au départ

	int count = atoi(argv[1]);
	pthread_t tid[count];

	for (int i = 0; i < count; ++i)
	{
		// Création d'un pointeur sur int, avec i comme valeur
		int *pi = malloc(sizeof(int));
		*pi = i;

		pthread_create(tid + i, NULL, fct, pi);
	}

	printf("Déblocage\n");
	pthread_mutex_unlock(&mutex); // Débloque le 1er

	// Attente de la fin des threads
	for (int i = 0; i < count; ++i)
	{
		pthread_join(tid[i], NULL);
	}

	pthread_mutex_destroy(&mutex);

	return 0;
}