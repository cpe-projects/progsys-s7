#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <semaphore.h>
#include <pthread.h>

sem_t sem1, sem2, sem3;

void *rdv1(void *arg) {
	printf("T1 : attend\n");
	sem_post(&sem1);
	sem_post(&sem1);
	sem_wait(&sem2);
	sem_wait(&sem3);

	printf("T1 : débloqué\n");

	return NULL;
}

void *rdv2(void *arg) {
	printf("T2 : attend\n");
	sem_post(&sem2);
	sem_post(&sem2);
	sem_wait(&sem1);
	sem_wait(&sem3);

	printf("T2 : débloqué\n");

	return NULL;
}

void *rdv3(void *arg) {
	printf("T3 : attend\n");
	sem_post(&sem3);
	sem_post(&sem3);
	sem_wait(&sem1);
	sem_wait(&sem2);

	printf("T3 : débloqué\n");

	return NULL;
}

int main(int argc, char const *argv[])
{

	sem_init(&sem1, 0, 0);
	sem_init(&sem2, 0, 0);
	sem_init(&sem3, 0, 0);

	pthread_t tid_A, tid_B, tid_C;
	
	pthread_create(&tid_A, NULL, rdv1, NULL);
	pthread_create(&tid_B, NULL, rdv2, NULL);
	pthread_create(&tid_C, NULL, rdv3, NULL);

	// Attente de la fin des threads
	pthread_join(tid_A, NULL);
	pthread_join(tid_B, NULL);
	pthread_join(tid_C, NULL);

	sem_destroy(&sem1);
	sem_destroy(&sem2);
	sem_destroy(&sem3);

	return 0;
}