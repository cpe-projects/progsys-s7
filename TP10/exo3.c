#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <pthread.h>

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void *threadA(void *arg) {
	int *result = (int *) arg;

	printf("Thread A travaille...\n");
	sleep(2);

	*result = 10;
	printf("Thread A a fini, il rend la main\n");

	pthread_mutex_unlock(&mutex); 

	return NULL;
}

void *threadB(void *arg) {
	int *result = (int *) arg;

	printf("Thread B attend...\n");
	pthread_mutex_lock(&mutex); 

	printf("Thread B travaille...\n");
	sleep(2);
	printf("Thread B utilise le résultat de Thead A : %d\n", *result);

	return NULL;
}

int main(int argc, char const *argv[])
{

	pthread_mutex_lock(&mutex);

	pthread_t tid_A, tid_B;
	int *result = malloc(sizeof(int));
	
	pthread_create(&tid_A, NULL, threadA, result);
	pthread_create(&tid_B, NULL, threadB, result);

	// Attente de la fin des threads
	pthread_join(tid_A, NULL);
	pthread_join(tid_B, NULL);

	pthread_mutex_destroy(&mutex);

	return 0;
}