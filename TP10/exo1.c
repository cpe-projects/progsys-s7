#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

int i = 0;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void *prod(void *arg) {
	printf("Hello from Prod\n");

	while(i < 100) {
		pthread_mutex_lock(&mutex);
		i++;
		printf("Prod: %d\n", i);
		pthread_mutex_unlock(&mutex);
		sched_yield();
	}

	return NULL;
}

void *conso(void *arg) {
	printf("Hello from Conso\n");

	while(i < 40) {
		pthread_mutex_lock(&mutex);
		printf("Conso: %d\n", i);
		pthread_mutex_unlock(&mutex);
		sched_yield();
	}

	return NULL;
}

int main(int argc, char const *argv[])
{

	pthread_t tid_prod, tid_conso;

	pthread_create(&tid_prod, NULL, prod, NULL);
	pthread_create(&tid_conso, NULL, conso, NULL);

	// Attente de la fin des 2 threads
	pthread_join(tid_prod, NULL);
	pthread_join(tid_conso, NULL);

	pthread_mutex_destroy(&mutex);

	return 0;
}