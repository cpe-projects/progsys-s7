#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

pthread_mutex_t mutex1 = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutex2 = PTHREAD_MUTEX_INITIALIZER;

void *t1(void *arg) {
	printf("Thread 1 is waiting...\n");
	pthread_mutex_unlock(&mutex1);
	pthread_mutex_lock(&mutex2);

	printf("Thread 1 running\n");

	return NULL;
}

void *t2(void *arg) {
	printf("Thread 2 is waiting...\n");
	sleep(2);
	pthread_mutex_unlock(&mutex2);
	pthread_mutex_lock(&mutex1);
	printf("Thread 2 running\n");

	return NULL;
}

int main(int argc, char const *argv[])
{

	pthread_t tid1, tid2;

	pthread_mutex_lock(&mutex1);
	pthread_mutex_lock(&mutex2);

	int* result = malloc(sizeof(int));
	pthread_create(&tid1, NULL, t1, result);
	pthread_create(&tid2, NULL, t2, result);
	pthread_join(tid1, NULL);
	pthread_join(tid2, NULL);

	pthread_mutex_destroy(&mutex1);
	pthread_mutex_destroy(&mutex2);

	return 0;
}