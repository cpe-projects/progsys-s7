#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <semaphore.h>
#include <pthread.h>

sem_t sem1, sem2;

void *rdv1(void *arg) {
	printf("T1 : attend\n");
	sem_post(&sem1);
	sem_wait(&sem2);

	printf("T1 : débloqué\n");

	return NULL;
}

void *rdv2(void *arg) {
	printf("T2 : attend\n");
	sem_post(&sem2);
	sem_wait(&sem1);

	printf("T2 : débloqué\n");

	return NULL;
}

int main(int argc, char const *argv[])
{

	sem_init(&sem1, 0, 0);
	sem_init(&sem2, 0, 0);

	pthread_t tid_A, tid_B;
	int *result = malloc(sizeof(int));
	
	pthread_create(&tid_A, NULL, rdv1, NULL);
	pthread_create(&tid_B, NULL, rdv2, NULL);

	// Attente de la fin des threads
	pthread_join(tid_A, NULL);
	pthread_join(tid_B, NULL);

	sem_destroy(&sem1);
	sem_destroy(&sem2);

	return 0;
}