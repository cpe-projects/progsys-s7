#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>

#include <fcntl.h>

int main(int argc, char const *argv[])
{

	/*******
	SORT
		Clavier <- fichier
		Ecran -> pipe[1] (entrée)

	GREP
		Clavier <- pipe[0] (sortie)
		Ecran -> pipe1[1]

	TAIL
		Clavier <- pipe1[0]
		Ecran -> sortie
	********/
	
	int p1[2], p2[2];

	if(pipe(p1) != 0) {
		perror("Error while opening pipe");
		exit(1);
	}

	if(pipe(p2) != 0) {
		perror("Error while opening pipe2");
		exit(2);
	}

	if(fork() == 0) { // Fils

		if(fork() == 0) { // Fils
			close(p2[0]);
			close(p2[1]);
			close(p1[0]);
		
			dup2(p1[1], 1);
			close(p1[1]);

			execlp("sort", "sort", "fichier", NULL);
		} else { // Père
			close(p1[1]);
			close(p2[0]);

			dup2(p1[0], 0);
			close(p1[0]);

			dup2(p2[1], 1);
			close(p2[1]);

			execlp("grep", "grep", "Mathias", NULL);
		}
	} else { // Père
		close(p1[0]);
		close(p1[1]);
		close(p2[1]);
		
		dup2(p2[0], 0);
		close(p2[0]);

		int fd = open("sortie", O_WRONLY);
		dup2(fd, 1);
		close(fd);

		execlp("tail", "tail", "-n", "5", NULL);
	}

	return 0;
}