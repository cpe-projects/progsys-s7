#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char const *argv[])
{

	/*******
	in=>{       }=>out
	
	in: sortie de "cat fichier"
	out: entrée de WC
	********/
	

	int status;
	int p[2];

	if(pipe(p) != 0) {
		perror("Error while opening pipe");
		exit(1);
	}

	if(fork() == 0) { // Fils
		close(p[1]);
		
		dup2(p[0], 0); // On remplace le "clavier" par la sortie du pipe
		close(p[0]);

		execlp("wc", "wc", NULL);
	} else { // Père
		close(p[0]);
		
		dup2(p[1], 1); // On remplace "l'écran" par l'entrée du pipe
		close(p[1]);

		execlp("cat", "cat", "fichier", NULL);
	}

	return 0;
}