#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <time.h>

#define BUF_SIZE 256
#define NB_NBRS 20
#define MAX_VALUE 500

#define WRITE_PIPE(P, B, S) if(write(P, B, S) != S) { perror("Error while writing pipe P"); exit(2); }					
#define READ_PIPE(P, B, S) if(read(P, B, S) != S) { perror("Error while reading pipe P"); exit(2); }

int main(int argc, char const *argv[])
{

	/**
	GENERATEUR
		ecrit dans evenPipe[1] et oddPipe[1]
		lit dans sumEvenPipe[0] et sumEvenPipe[0]
	
	EVENFILTER
		lit dans evenPipe[0]
		ecrit dans sumEvenPipe[1]
	
	ODDFILTER
		lit dans oddPipe[0]
		ecrit dans sumOddPipe[1]

	**/	

	int evenPipe[2], oddPipe[2],
		sumEvenPipe[2], sumOddPipe[2];

	char buffer[BUF_SIZE];
	srand(time(NULL));

	if(pipe(evenPipe) != 0) {
		perror("Error while opening pipe");
		exit(1);
	}
	if(pipe(oddPipe) != 0) {
		perror("Error while opening pipe");
		exit(1);
	}
	if(pipe(sumEvenPipe) != 0) {
		perror("Error while opening pipe");
		exit(1);
	}
	if(pipe(sumOddPipe) != 0) {
		perror("Error while opening pipe");
		exit(1);
	}

	if(fork() == 0) { // EVENFILTER

		close(oddPipe[0]);
		close(oddPipe[1]);
		close(sumOddPipe[0]);
		close(sumOddPipe[1]);

		close(evenPipe[1]);
		close(sumEvenPipe[0]);

		int sum = 0;
		int current;

		do {
			READ_PIPE(evenPipe[0], buffer, BUF_SIZE);
			current = atoi(buffer);
			sum += current;
		} while(current != -1);

		close(evenPipe[0]);

		sprintf(buffer, "%d", sum);
		WRITE_PIPE(sumEvenPipe[1], buffer, BUF_SIZE);
		close(sumEvenPipe[1]);

		exit(0);

	} 

	if(fork() == 0) { // ODDFILTER

		close(evenPipe[0]);
		close(evenPipe[1]);
		close(sumEvenPipe[0]);
		close(sumEvenPipe[1]);
		close(oddPipe[1]);
		close(sumOddPipe[0]);


		int sum = 0;
		int current = 0;

		do {
			READ_PIPE(oddPipe[0], buffer, BUF_SIZE);
			current = atoi(buffer);
			sum += current;
		} while(current != -1);

		close(oddPipe[0]);

		sprintf(buffer, "%d", sum);
		WRITE_PIPE(sumOddPipe[1], buffer, BUF_SIZE);
		close(sumOddPipe[1]);

		exit(0);

	}
	
	// GENERATOR
	
	printf("Generator\n");
	close(oddPipe[0]);
	close(evenPipe[0]);
	close(sumOddPipe[1]);
	close(sumEvenPipe[1]);

	int current = 0;
	for (int i = 0; i < NB_NBRS; i++)
	{
		current = rand() % MAX_VALUE;
		sprintf(buffer, "%d", current);

		if(current % 2 == 0) {
			WRITE_PIPE(evenPipe[1], buffer, BUF_SIZE)
		} else {
			WRITE_PIPE(oddPipe[1], buffer, BUF_SIZE)
		}

	}

	WRITE_PIPE(evenPipe[1], "-1", BUF_SIZE)
	WRITE_PIPE(oddPipe[1], "-1", BUF_SIZE)

	close(evenPipe[1]);
	close(oddPipe[1]);

	int sum = 0;

	READ_PIPE(sumOddPipe[0], buffer, BUF_SIZE);
	close(sumOddPipe[0]);

	sum = atoi(buffer);

	READ_PIPE(sumEvenPipe[0], buffer, BUF_SIZE);
	close(sumEvenPipe[0]);

	sum += atoi(buffer);

	printf("Somme total : %d\n", sum);

	return 0;
}