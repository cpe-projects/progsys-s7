#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char const *argv[])
{
	

	int status;
	int p[2];

	char buffer[256];

	if(pipe(p) != 0) {
		perror("Error while opening pipe");
		exit(1);
	}

	if(fork() == 0) { // Fils
		close(p[0]);
		
		if(write(p[1], "Laurent", 7) != 7) {
			perror("Error while writing into pipe");
			exit(2);
		}

		exit(0);
	} else { // Père
		close(p[1]);

		if(read(p[0], buffer, 7) != 7) {
			perror("Error while reading pipe");
			exit(3);
		}

		printf("Hello %s\n", buffer);
	}

	wait(&status);
	return 0;
}